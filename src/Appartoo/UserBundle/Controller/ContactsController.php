<?php
// src/OC/PlatformBundle/Controller/AdvertController.php

namespace Appartoo\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class ContactsController extends Controller
{
  public function indexAction()
  {
    $user = $this->getUser();
    return $this->render('AppartooUserBundle:Contacts:index.html.twig', array(
                'user' => $user
              ));
  }
    
    /**
     * Show the user's contacts
     */
    public function listAction()
    {
        $user = $this->getUser();

        return $this->render('AppartooUserBundle:Contacts:show_contacts.html.twig', array(
                'user' => $user
              ));
    }

    public function showAction($id, Request $request)
    {
      $userManager = $this->container->get('fos_user.user_manager');
      $contact = $userManager->finduserBy(array('id' => $id));
      if($contact) {
        return $this->render('AppartooUserBundle:Contacts:details.html.twig', array(
          'contact' => $contact
        ));
      }
      $request->getSession()->getFlashBag()->add('danger', 'Contact inexistant.');
    }
  

  public function addAction(Request $request)
  {
    if ($request->isMethod('POST')) {
      //traitement des données reçues
      $email = $request->request->get('email');
      $userManager = $this->container->get('fos_user.user_manager');
      //recherche du contact
      $contact = $userManager->finduserBy(array('email' => $email));

      if($contact) { // contact existe en BD
        $user = $this->getUser();
        //on vérifie qu'il ne l'a pas déjà en contact
        if($user->getContacts()->contains($contact) === false) {
          $user->addContact($contact);
          $request->getSession()->getFlashBag()->add('success', 'Contact bien enregistré.');
          $em = $this->getDoctrine()->getEntityManager();
          $em->flush(); // sauvegarde des changements en BD
        } else { // on l'a déjà
          $request->getSession()->getFlashBag()->add('info', 'Contact déjà existant.');
        }

        // Puis on redirige vers la page de visualisation des contacts
        return $this->redirect($this->generateUrl('appartoo_user_contacts_view'));
      }
      //email non trouvé en BD
      $request->getSession()->getFlashBag()->add('danger', 'Contact inexistant.');
    }

    // Si on n'est pas en POST, alors on affiche le formulaire
    return $this->render('AppartooUserBundle:Contacts:add_contact.html.twig');
  }

  public function deleteAction($id, Request $request)
  {
    // On récupère l'EntityManager
    $em = $this->getDoctrine()->getManager();

    // On récupère l'entité correspondant à l'id $id
    $contact = $em->getRepository('AppartooUserBundle:User')->find($id);

    // Si l'annonce n'existe pas, on affiche une erreur 404
    if ($contact == null) {
      throw $this->createNotFoundException("Le contact d'id ".$id." n'existe pas.");
    }

    if ($request->isMethod('POST')) {
      // Si la requête est en POST, on deletea le contact
      $request->getSession()->getFlashBag()->add('info', 'Contact bien supprimée.');
      $user = $this->getUser();
      $user->removeContact($contact);
      $em->flush();
      // Puis on redirige vers l'accueil
      return $this->redirect($this->generateUrl('appartoo_user_homepage'));
    }

    // Si la requête est en GET, on affiche une page de confirmation avant de delete
    return $this->render('AppartooUserBundle:Contacts:delete.html.twig', array(
      'contact' => $contact
    ));
  }
}