<?php
// src/Acme/UserBundle/Entity/User.php

namespace Appartoo\UserBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string $adresse
     *
     * @ORM\Column(name="adresse", type="string", length=255)
     * @Assert\NotBlank(message="Please enter your adress.", groups={"Registration", "Profile"})
     * @Assert\Length(
     *     min=3,
     *     max="255",
     *     minMessage="The adress is too short.",
     *     maxMessage="The adress is too long.",
     *     groups={"Registration", "Profile"}
     * )
     */
    protected $adresse;

    /**
     * @var string $telephone
     * @ORM\Column(name="telephone", type="string", length=10)
     * @Assert\NotBlank(message="Please enter your phone number.", groups={"Registration", "Profile"})
     * @Assert\Length(
     *     min=10,
     *     max="10",
     *     minMessage="The phone number is too short.",
     *     maxMessage="The phone number is too long.",
     *     groups={"Registration", "Profile"}
     * )
     */
    protected $telephone;

    /**
     * @var string $site
     *
     * @ORM\Column(name="site", type="string", length=255)
     * @Assert\Length(
     *     min=5,
     *     max="255",
     *     minMessage="The url is too short.",
     *     maxMessage="The url is too long.",
     *     groups={"Registration", "Profile"}
     * )
     */
    protected $site;

    /**
     * @ORM\ManyToMany(targetEntity="User", cascade={"persist"})
     */
    protected $contacts;

    public function __construct()
    {
        parent::__construct();
        // your own logic
        $this->addRole('ROLE_USER');
        $this->contacts = new ArrayCollection();
    }

    /** 
     * Get adresse
     * @return string
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set adresse
     *
     * @param string $adresse
     * @return User
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /** 
     * Get telephone
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     * @return User
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /** 
     * Get site
     * @return string
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * Set site
     *
     * @param string $site
     * @return User
     */
    public function setSite($site)
    {
        $this->site = $site;

        return $this;
    }

    public function addContact(User $contact)
    {
        $this->contacts[] = $contact;
        return $this;
    }

    public function removeContact(User $contact)
    {
        $this->contacts->removeElement($contact);
    }

    public function getContacts()
    {
        return $this->contacts;
    }
}