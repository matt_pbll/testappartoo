TestAppartoo
============

Projet de test sur Symfony2 pour le stage chez Appartoo.

But du projet : développement d'un carnet d'adresse mutli-utilisateurs sur Symfony2. 

Fonctionnalités implémentées par le programme : 
- Identification (Inscription/Connexion/Déconnexion) de l'utilisateur par login/mot de passe (voir FOSUserBundle).
- Afficher/Modifier ses informations (email / adresse / téléphone / site web).
- Ajouter/Afficher/Lister/Supprimer les membres de son carnet d'adresses (membres qui devront être préalablement inscrit sur le site).
